package com.softtek.academia.servlet.HandsOn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UniqueConnection {
	private static UniqueConnection conexion;
	Connection conn1;
	
	private UniqueConnection() {
		try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                conn1 = DriverManager.getConnection(
                        "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
                
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		
	}
	
	public static UniqueConnection getInstance() {
		if (conexion == null) {
			System.out.println("Creando objeto único...");
			conexion = new UniqueConnection();
		}
		return conexion;
	}
	
	public List<Device> creandoLista() throws SQLException {
		String query = "SELECT * FROM DEVICE";
		List<Device> result = new ArrayList<>();
		
		PreparedStatement preparedStatemet = conn1.prepareStatement(query);
		ResultSet resultSet = preparedStatemet.executeQuery();
		while(resultSet.next()) {
			int deviceId = resultSet.getInt("deviceId");
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            int manufacturerId = resultSet.getInt("manufacturerId");
            int colorId = resultSet.getInt("colorId");
            String comments = resultSet.getString("comments");
            result.add(new Device(deviceId, name, description, manufacturerId, colorId, comments));
            
		}
		
		return result;
	}
}
