package com.softtek.academia.servlet.HandsOn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException{
    	String query = "Select * FROM DEVICE";
    	System.out.println("Vamos a mostrar diferentes resultados en un momento");
    	UniqueConnection UC = UniqueConnection.getInstance();
    	
    	System.out.println("Lista de todos los Devices de la base de datos: ");
    	System.out.println();
    	List<Device> result = UC.creandoLista();
    	result.forEach(System.out::println);
    	
    	System.out.println();
    	System.out.println("Lista de dispositivos que sean 'Laptop': ");
    	List<String> listaNombres = result.stream()
    			.map(Device::getName)
    			.filter(device -> device.contains("Laptop"))
    			.collect(Collectors.toList());
    	listaNombres.forEach(System.out::println);
    	
    	System.out.println();
    	System.out.println("Cantidad de registros con id = 3");
    	long cantidadIds = result.stream()
    			.map(Device::getManufacturerId)
    			.filter(id -> id == 3).count();
    	System.out.println(cantidadIds);
    	
    	System.out.println();
    	System.out.println("Lista de Devices pero que tengan el colorId = 1");
    	List<Device> listaColores = result.stream()
    			.filter(color -> color.getColorId() == 1)
    			.collect(Collectors.toList());
    	listaColores.forEach(System.out::println);
    	
    	System.out.println();
    	System.out.println("Mapa que usa el DeviceId como key y el Device object como value: ");
    	Map<Integer, List<Device>> deviceMap = result.stream()
    			.collect(Collectors.groupingBy(Device::getDeviceId));
    	System.out.println(deviceMap);
    	
    }
}
